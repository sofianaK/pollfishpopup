package gr.akoukio.webviewoverlaylibrary;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import gr.akoukio.pollfishpopup.PopUpManager;

public class MainActivity extends AppCompatActivity implements PopUpManager.Callback {

    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.button_id);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                button.setEnabled(false);
                final FrameLayout parentLayout = findViewById(R.id.main_layout);
                List<String> inputParamsList;
                List<String> outputParamsList;
                outputParamsList = new ArrayList<String>();
                inputParamsList = new ArrayList<String>();
                inputParamsList.add("param__1");
                inputParamsList.add("param__2");
                outputParamsList.add("param__3");
                outputParamsList.add("param__4");
                outputParamsList.add("param__5");
                PopUpManager.showPopUp(parentLayout,inputParamsList,  outputParamsList, MainActivity.this);
            }
        });
    }

    @Override
    public void onPageLoaded() {
        button.setEnabled(true);
        Toast.makeText(this, "onPageLoaded", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPageClosed(List<String> outputParamsList) {

        if(outputParamsList.size()>= 3){
            Toast.makeText(this, "onPageClosed -> " + outputParamsList.get(0) + ", " + outputParamsList.get(1) + ", " + outputParamsList.get(2), Toast.LENGTH_LONG).show();
        }




    }
}

