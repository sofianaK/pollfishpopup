package gr.akoukio.pollfishpopup;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;


public class PopUpManager {

    public static void showPopUp(final ViewGroup parentView, List<String> inputParamsList, final List<String> outputParamsList, final Callback callback) {
        LayoutInflater vi = (LayoutInflater) parentView.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View popupRoot = vi.inflate(R.layout.layout_popup, null);
        popupRoot.setVisibility(View.GONE);
        WebView webView = popupRoot.findViewById(R.id.webview_id);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                popupRoot.setVisibility(View.VISIBLE);
                slideRight(popupRoot);
                callback.onPageLoaded();
            }
        });
        webView.loadUrl("https://www.pollfish.com");
        Button closeButton = popupRoot.findViewById(R.id.close_button_id);
        TextView param1TextView = popupRoot.findViewById(R.id.param1_id);
        TextView param2TextView = popupRoot.findViewById(R.id.param2_id);

        if(outputParamsList.size()>= 2){

            param1TextView.setText(inputParamsList.get(0));
            param2TextView.setText(inputParamsList.get(1));
        }

        parentView.addView(popupRoot, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                slideLeft(popupRoot);
                parentView.removeView(popupRoot);

                    callback.onPageClosed(outputParamsList);

            }
        });
    }

    private static void slideRight(View view) {
        Animation inFromRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(500);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        view.startAnimation(inFromRight);
    }

    private static void slideLeft(View view) {
        Animation inFromRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(500);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        view.startAnimation(inFromRight);
    }

    public interface Callback {
        void onPageLoaded();

        void onPageClosed(List<String> outputParamsList);
    }
}
